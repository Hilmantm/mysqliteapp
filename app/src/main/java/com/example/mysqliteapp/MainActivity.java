package com.example.mysqliteapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;

import com.example.mysqliteapp.db.DatabaseHelper;
import com.example.mysqliteapp.model.Student;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    DatabaseHelper dbHelper;
    EditText editName, editSurname, editMarks, editTextId;
    Button btnAddData, btnViewAll, btnDelete, btnViewUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHelper = new DatabaseHelper(this);
        initComponents();
    }

    private void initComponents() {
        editName = findViewById(R.id.editText_name);
        editSurname = findViewById(R.id.editText_username);
        editMarks = findViewById(R.id.editText_marks);
        editTextId = findViewById(R.id.editText_id);

        btnAddData = findViewById(R.id.add_button);
        btnDelete = findViewById(R.id.delete_button);
        btnViewAll = findViewById(R.id.view_button);
        btnViewUpdate = findViewById(R.id.update_button);

        btnAddData.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnViewAll.setOnClickListener(this);
        btnViewUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.delete_button:
                delete();
                break;
            case R.id.add_button:
                add();
                break;
            case R.id.view_button:
                viewClick();
                break;
            case R.id.update_button:
                update();
                break;
        }
    }

    private void update() {
        Student student = new Student();
        student.setName(editName.getText().toString());
        student.setSurname(editSurname.getText().toString());
        student.setMarks(Integer.parseInt(editMarks.getText().toString()));
        boolean update = dbHelper.updateData(editTextId.getText().toString(), student);
        if(update) {
            Toast.makeText(MainActivity.this, "Data berhasil diubah", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, "Data gagal diubah", Toast.LENGTH_SHORT).show();
        }
    }

    private void viewClick() {
        Cursor res = dbHelper.getAllData();
        if(res.getCount() == 0) {
            showMessage("Error", "Not Found any data");
            return;
        }
        StringBuffer stringBuffer = new StringBuffer();
        while (res.moveToNext()) {
            stringBuffer.append("Id : " + res.getString(0) + "\n");
            stringBuffer.append("Name : " + res.getString(1) + "\n");
            stringBuffer.append("Surname : " + res.getString(2) + "\n");
            stringBuffer.append("Marks : " + res.getString(3) + "\n");
        }

        showMessage("Data", stringBuffer.toString());
    }

    private void add() {
        Student student = new Student();
        student.setName(editName.getText().toString());
        student.setSurname(editSurname.getText().toString());
        student.setMarks(Integer.parseInt(editMarks.getText().toString()));
        boolean insert = dbHelper.insertData(student);
        if(insert) {
            Toast.makeText(MainActivity.this, "Data berhasil ditambahkan", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, "Data gagal ditambahkan", Toast.LENGTH_SHORT).show();
        }
    }

    private void delete() {
        Integer deleteRows = dbHelper.deleteData(editTextId.getText().toString());
        if(deleteRows > 0) {
            Toast.makeText(MainActivity.this, "Data berhasil dihapus", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, "Data gagal dihapus", Toast.LENGTH_SHORT).show();
        }
    }

    private void showMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }
}
