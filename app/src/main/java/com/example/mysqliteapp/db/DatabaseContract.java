package com.example.mysqliteapp.db;

import android.provider.BaseColumns;

public class DatabaseContract {

    static final String DATABASE_NAME = "db_student";
    static final String TABLE_NAME = "student_table";

    class StudentField implements BaseColumns {

        public static final String NAME = "NAME";
        public static final String SURNAME = "SURNAME";
        public static final String MARKS = "MARKS";

    }

}
